package cz.uhk.fim.shoppingcart.app;

import cz.uhk.fim.shoppingcart.gui.MainFrame;

import javax.swing.*;

/**
 * Class App for running our application
 */
public class App {
    /**
     * Main method
     * @param args passed arguments
     */
    public static void main(String[] args) {
//        int[] counts = new int[1];
//        for (int i = 0; i < 100; i++)
//        {
//            new Thread(new Runnable()
//            {
//                @Override
//                public void run()
//                {
//                    for (int j = 0; j < 100; j++)
//                    {
//                        counts[0]++;
//                    }
//                    System.out.println(counts[0]);
//                }
//            }).start();
//        }
//
//        System.out.println(counts[0]);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainFrame();
            }
        });
    }
}
