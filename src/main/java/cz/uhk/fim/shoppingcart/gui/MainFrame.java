package cz.uhk.fim.shoppingcart.gui;

import cz.uhk.fim.shoppingcart.model.ShoppingCart;
import cz.uhk.fim.shoppingcart.model.ShoppingCartItem;
import cz.uhk.fim.shoppingcart.persistence.JsonPersistenceFactory;
import cz.uhk.fim.shoppingcart.persistence.Persistence;

import javax.print.attribute.standard.JobKOctets;
import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.time.LocalDateTime;

public class MainFrame extends JFrame
{
    private ShoppingCartTableModel model;
    JLabel boughtPriceLabel;
    JLabel totalPriceLabel;
    ShoppingCart shoppingCart;
    JList list;

    public MainFrame()
    {
        setTitle("Shopping Cart");
        setSize(640, 480);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        initUi();
        shoppingCart = new ShoppingCart();
        shoppingCart.addItem(new ShoppingCartItem("Rajcata",10,1));
        shoppingCart.addItem(new ShoppingCartItem("Pivo",20,5));
        model.setShoppingCart(shoppingCart);

        double sum = 0;
        for(ShoppingCartItem item : shoppingCart.getAllItems())
        {
            sum += item.getNumberOfPieces() * item.getPricePerPiece();
        }

        totalPriceLabel.setText(Double.toString(sum));

        model.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                list.setModel(new ShoppingCartListModel(shoppingCart));
            }
        });

        list.setModel(new ShoppingCartListModel(shoppingCart));
    }

    private void initUi()
    {
        // JTable
        // JScrollPane
        // JPanel
        // JTextField
        // JSpinner
        // JButton
        // JLabel

        // FlowLayout
        // BorderLayout

        JTabbedPane tabbedPane = new JTabbedPane();
        JPanel firstTabPanel = new JPanel(new BorderLayout());
        JPanel secondTabPanel = new JPanel();
        tabbedPane.addTab("Table", firstTabPanel);
        tabbedPane.addTab("List", secondTabPanel);

        add(tabbedPane, BorderLayout.CENTER);

        list = new JList();
        secondTabPanel.add(list);

        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.X_AXIS));
        JLabel titleLabel = new JLabel("Název:");
        JLabel priceLabel = new JLabel("Cena/ks:");
        JLabel amountLabel = new JLabel("Počet kusů:");
        JTextField titleField = new JTextField();
        JTextField priceField = new JTextField();
        JTextField amountField = new JTextField();
        JButton submitButton = new JButton("Přidat");
        JLabel timeLabel = new JLabel("");
        JButton openButton = new JButton("Otevřít");
        JButton saveButton = new JButton("Uložit");

        inputPanel.add(titleLabel);
        inputPanel.add(titleField);
        inputPanel.add(priceLabel);
        inputPanel.add(priceField);
        inputPanel.add(amountLabel);
        inputPanel.add(amountField);
        inputPanel.add(submitButton);
        inputPanel.add(timeLabel);
        inputPanel.add(openButton);
        inputPanel.add(saveButton);

        firstTabPanel.add(inputPanel,BorderLayout.NORTH);

        submitButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                shoppingCart.addItem(
                        new ShoppingCartItem(
                                titleField.getText(),
                                Integer.parseInt(priceField.getText()),
                                Integer.parseInt(amountField.getText())
                        ));
                model.fireTableDataChanged();
            }
        });

        saveButton.addMouseListener(new MouseAdapter()
      {
          @Override
          public void mouseClicked(MouseEvent e)
          {
              JFileChooser fileChooser = new JFileChooser();
              int result = fileChooser.showOpenDialog(MainFrame.this);

              if (result == JFileChooser.APPROVE_OPTION)
              {
                  File file =fileChooser.getSelectedFile();
                  JsonPersistenceFactory jsonPersistenceFactory = new JsonPersistenceFactory(file.getPath());
                  Persistence persistence = jsonPersistenceFactory.GetPersistence();
                  persistence.saveShoppingCart(shoppingCart);
              }
          }
      });


                JTable table = new JTable();
        firstTabPanel.add(new JScrollPane(table), BorderLayout.CENTER);
        model = new ShoppingCartTableModel();
        table.setModel(model);

        totalPriceLabel = new JLabel();
        firstTabPanel.add(totalPriceLabel, BorderLayout.SOUTH);

        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                while (true)
                {
                    try
                    {
                        Thread.sleep(1000);
                        SwingUtilities.invokeLater(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                timeLabel.setText(LocalDateTime.now().toString());
                            }
                        });
                    } catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
