package cz.uhk.fim.shoppingcart.gui;

import cz.uhk.fim.shoppingcart.model.ShoppingCart;
import cz.uhk.fim.shoppingcart.model.ShoppingCartItem;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.HashMap;

public class ShoppingCartTableModel extends AbstractTableModel
{
    private ShoppingCart shoppingCart;

    public void setShoppingCart(ShoppingCart shoppingCart)
    {
        this.shoppingCart = shoppingCart;
        fireTableDataChanged();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        return columnIndex != 3;
    }

    @Override
    public String getColumnName(int column)
    {
        switch (column)
        {
            case 0:
                return "Název";
            case 1:
                return "Cena za kus";
            case 2:
                return "Počet kusů";
            case 3:
                return "Celková cena";
            case 4:
                return "Zakoupeno";
            case 5:
                return "English";
            default:
                return "?";
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        switch (columnIndex)
        {
            case 0:
                return String.class;
            case 1:
                return Double.class;
            case 2:
                return Integer.class;
            case 3:
                return Double.class;
            case 4:
                return Boolean.class;
            case 5:
                return String.class;
            default:
                return null;
        }
    }

    @Override
    public int getRowCount()
    {
        if(shoppingCart == null)
            return 0;
        else
            return shoppingCart.getCount();
    }

    @Override
    public int getColumnCount()
    {
        return 6;
    }

    HashMap<String, String> dictionary = new HashMap<>();

    // Pozor, tato metoda může běžet dlouho
    private String WebTranslate(String czech)
    {
        return "NOT FOUND";
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        ShoppingCartItem item = shoppingCart.getItem(rowIndex);
        switch (columnIndex) {
            case 0:
                return item.getTitle();
            case 1:
                return item.getPricePerPiece();
            case 2:
                return item.getNumberOfPieces();
            case 3:
                return item.getNumberOfPieces() * item.getPricePerPiece();
            case 4:
                return item.isPurchased();
            case 5:
                if(dictionary.containsKey(item.getTitle()))
                {
                    return dictionary.get(item.getTitle());
                }
                else {
                    new Thread(new TranslatorRunnable(dictionary)).start();
//                    TranslatorRunnable není implementováno!

//                    String english = WebTranslate(item.getTitle());
//                    dictionary.put(item.getTitle(),english);
//                    return english;
                }
            default:
                return "?";
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {
        ShoppingCartItem item = shoppingCart.getItem(rowIndex);
        switch (columnIndex) {
            case 0:
                item.setTitle((String) aValue);
                break;
            case 1:
                item.setPricePerPiece((Double) aValue);
                break;
            case 2:
                item.setNumberOfPieces((Integer) aValue);
                break;
            case 4:
                item.setPurchased((Boolean) aValue);
                break;
            default:
                break;
        }
        fireTableCellUpdated(rowIndex,columnIndex);
    }
}
